$(function () {

	"use strict";

	var fullHeight = function () {

		$('.js-fullheight').css('height', $(window).height());
		$(window).on("risize", function () {
			$('.js-fullheight').css('height', $(window).height());
		});


	};
	fullHeight();

	$('#sidebarCollapse').on('click', function () {
		$('#sidebar').toggleClass('active');
		if ($(window).width() < 576) {
			$(".nav-search").toggleClass("search-toggle");
		}
		$(".navbar").toggleClass("navbar-toggle");
	});

	$("#sidebar ul").on("mouseover", function (e) {
		$(".sidebar-modal").show();
		$(".caret-top").show();
		if ($(e.target).attr("data-toggle") == "modal") {
			$(".sidebar-modal").html($(e.target).html());
		}
		else if ($(e.target)[0].tagName == "LI") {
			$(".sidebar-modal").html($(e.target).children().html());
		}
	});

	$("#sidebar").on("mouseleave", function () {
		$(".sidebar-modal").hide();
		$(".caret-top").hide();
	})

	$("#content").on("mouseover", function (e) {
		if ($(e.target).hasClass("sub_image") == true) {
			$(".image_stage").attr("src", $(e.target).attr("src"));
		}
	});

	$(window).on("scroll", function () {
		if ($(document).scrollTop() > 2000) {
			$(".product-detail").addClass("scroll-state");
		}
		else {
			$(".product-detail").removeClass("scroll-state");
		}
	})

});

$(window).on("scroll", function () {
	
	if ($(window)[0].pageYOffset >= 323) {
		$("#navbar").addClass("sticky");
		$(".top-scroll").css("display", "block");
	}
	if ($(window)[0].pageYOffset < 323) {
		$("#navbar").removeClass("sticky");
		$(".top-scroll").css("display", "none");
	}
});


// // Add smooth scrolling to all links
// $("#top").on('click', function (event) {

// 	// Make sure this.hash has a value before overriding default behavior
// 	if (this.hash !== "") {
// 		// Prevent default anchor click behavior
// 		event.preventDefault();

// 		// Store hash
// 		var hash = this.hash;

// 		// Using jQuery's animate() method to add smooth page scroll
// 		// The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
// 		$('html, body').animate({
// 			scrollTop: $(hash).offset().top
// 		}, 800, function () {

// 			// Add hash (#) to URL when done scrolling (default click behavior)
// 			window.location.hash = hash;
// 		});
// 	} // End if
// });

// window.onscroll = function() {myFunction()};

// var navbar = document.getElementById("navbar");
// var sticky = navbar.offsetTop;

// function myFunction() {
//   if (window.pageYOffset >= sticky) {
//     navbar.classList.add("sticky")
//   } else {
//     navbar.classList.remove("sticky");
//   }
// }