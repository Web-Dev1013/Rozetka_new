import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AllComponent } from './all/all.component';
import { SpecComponent } from './spec/spec.component';
import { ReviewsComponent } from './reviews/reviews.component';
import { PhotoComponent } from './photo/photo.component';
import { QuestionsComponent } from './questions/questions.component';
import { Routes, RouterModule } from '@angular/router';

import { from } from 'rxjs';
import { ProductDetailsComponent } from './product-details.component';

const routes: Routes= [
  {path: '', component: ProductDetailsComponent, children:[
    {path: '', redirectTo:'all', pathMatch: 'full'},
    {path: 'all', component:AllComponent },
    {path: 'spec', component: SpecComponent},
    {path: 'reviews', component: ReviewsComponent },
    {path: 'photos', component: PhotoComponent},
    {path: 'questions', component: QuestionsComponent}
  ]},
  
]

@NgModule({
  declarations: [AllComponent, SpecComponent, ReviewsComponent, PhotoComponent, QuestionsComponent, ProductDetailsComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  
})
export class ProductDetailsModule { }
