import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ProductListService } from '../../service/product-list.service';
import { Product } from '../../model/product';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {
  products: Product[];
  constructor(private router: Router, private productListService: ProductListService) { }

  ngOnInit(): void {
    this.productListService.getAllProducts().subscribe((products: Product[]) => {
      this.products = products;
    })
  }
  
  public gotoProductDetail(url, productId) {
    this.router.navigate([url, productId]).then((e) => {
      if(e) {
        console.log('Navigation is successful!');
      }
      else {
        console.log('Navigation is failed!');
      }
    })
  }
}
