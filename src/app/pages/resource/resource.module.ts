import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AgmCoreModule } from '@agm/core';
import { Routes, RouterModule } from '@angular/router';

import { ResourceComponent } from './resource.component';
import { ContactComponent } from './contact/contact.component';
import { AnswersComponent } from './answers/answers.component';
import { CreditComponent } from './credit/credit.component';
import { PaymentComponent } from './payment/payment.component';
import { WarrantyComponent } from './warranty/warranty.component';
import { AboutComponent } from './about/about.component';
import { CooperationComponent } from './cooperation/cooperation.component';
import { VacaniesComponent } from './vacanies/vacanies.component';
import { ServiceCentresComponent } from './service-centres/service-centres.component';
import { GiftComponent } from './gift/gift.component';
import { SiteTermsComponent } from './site-terms/site-terms.component';


const apiKey:string = "AIzaSyCE0nvTeHBsiQIrbpMVTe489_O5mwyqofk";

const routes: Routes= [
  {path: '', component: ResourceComponent, children:[
    {path: '', redirectTo:'contact', pathMatch: 'full'},
    {path: 'contact', component:ContactComponent },
    {path: 'answers', component: AnswersComponent},
    {path: 'credit', component: CreditComponent },
    {path: 'payment', component: PaymentComponent},
    {path: 'warranty', component: WarrantyComponent},
    {path: 'about', component:AboutComponent },
    {path: 'cooperation', component:CooperationComponent },
    {path: 'vacanies', component: VacaniesComponent },
    {path: 'serviceCentres', component: ServiceCentresComponent },
    {path: 'gift', component: GiftComponent },
    {path: 'siteTerms', component: SiteTermsComponent }
  ]},
  
]

@NgModule({
  declarations: [
    ResourceComponent,
    ContactComponent,
    AnswersComponent,
    CreditComponent,
    PaymentComponent,
    WarrantyComponent,
    AboutComponent,
    CooperationComponent,
    VacaniesComponent,
    ServiceCentresComponent,
    GiftComponent,
    SiteTermsComponent,
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    AgmCoreModule.forRoot({
      apiKey: apiKey,
      libraries: ['places']
    })
  ]
})
export class ResourceModule { }
