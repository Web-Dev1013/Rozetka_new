import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VacaniesComponent } from './vacanies.component';

describe('VacaniesComponent', () => {
  let component: VacaniesComponent;
  let fixture: ComponentFixture<VacaniesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VacaniesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VacaniesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
