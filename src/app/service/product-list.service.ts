import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ProductListService {
  API_URL: string = 'api/';
  constructor(private httpClient: HttpClient) { }

  getProducts_list() {
    return this.httpClient.get(this.API_URL + 'product_list');
  }

  getProduct(productId) {
    return this.httpClient.get(`{this.API_URL + 'products'}/${productId}`);
  }

  getAllProducts() {
    return this.httpClient.get(`{this.API_URL + 'products'}`);
  }
}
