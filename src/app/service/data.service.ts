import { Injectable } from '@angular/core';
import { InMemoryDbService } from 'angular-in-memory-web-api';

@Injectable({
  providedIn: 'root'
})
export class DataService implements InMemoryDbService{

  constructor() { }
  createDb() {
    let product_list = [
      {cat_id: 1, cat_name: 'Laptops and Computers', subcat1_name: 'Popular Categories', subcat2: 'Laptops'},
      {cat_id: 2, cat_name: 'Laptops and Computers', subcat1_name: 'Popular Categories', subcat2: 'Tablets'},
      {cat_id: 3, cat_name: 'Laptops and Computers', subcat1_name: 'Popular Categories', subcat2: 'Video cards'},
      {cat_id: 4, cat_name: 'Laptops and Computers', subcat1_name: 'Popular Categories', subcat2: 'Hard drives and dis arrays'},
      {cat_id: 5, cat_name: 'Laptops and Computers', subcat1_name: 'Popular Categories', subcat2: 'Monitors'},
      {cat_id: 6, cat_name: 'Laptops and Computers', subcat1_name: 'Popular Categories', subcat2: 'Computers, nettops, monoblocks'},
      {cat_id: 7, cat_name: 'Laptops and Computers', subcat1_name: 'Popular Categories', subcat2: 'Processors'},
      {cat_id: 8, cat_name: 'Laptops and Computers', subcat1_name: 'Popular Categories', subcat2: 'SSD'},
      {cat_id: 9, cat_name: 'Laptops and Computers', subcat1_name: 'Popular Categories', subcat2: 'Playstation 5'},
      {cat_id: 10, cat_name: 'Laptops and Computers', subcat1_name: 'Popular Categories', subcat2: 'MFP/Printers'},
      {cat_id: 11, cat_name: 'Laptops and Computers', subcat1_name: 'Popular Categories', subcat2: 'Memory'},
      {cat_id: 12, cat_name: 'Laptops and Computers', subcat1_name: 'Popular Categories', subcat2: 'motherboards'},
      {cat_id: 13, cat_name: 'Laptops and Computers', subcat1_name: 'Popular Categories', subcat2: 'Mouse'},
      {cat_id: 14, cat_name: 'Laptops and Computers', subcat1_name: 'Popular Categories', subcat2: 'Routers'},
      {cat_id: 15, cat_name: 'Laptops and Computers', subcat1_name: 'Popular Categories', subcat2: 'Acustic systems'},
      {cat_id: 16, cat_name: 'Laptops and Computers', subcat1_name: 'Popular Categories', subcat2: 'Keyboards'},
      {cat_id: 17, cat_name: 'Laptops and Computers', subcat1_name: 'Popular Categories', subcat2: 'Power supplies'},
      {cat_id: 18, cat_name: 'Laptops and Computers', subcat1_name: 'Popular Categories', subcat2: 'Housings'},
      {cat_id: 19, cat_name: 'Laptops and Computers', subcat1_name: 'Popular Categories', subcat2: 'Projectors'},
      {cat_id: 20, cat_name: 'Laptops and Computers', subcat1_name: 'Popular Categories', subcat2: 'USB flash memory'},
      {cat_id: 21, cat_name: 'Laptops and Computers', subcat1_name: 'Popular Categories', subcat2: 'Uninterrupitle Power Supplies'},
      {cat_id: 22, cat_name: 'Laptops and Computers', subcat1_name: 'Popular Categories', subcat2: 'Surge Protectors'},
      {cat_id: 23, cat_name: 'Laptops and Computers', subcat1_name: 'Laptops', subcat2: 'Asus'},
      {cat_id: 24, cat_name: 'Laptops and Computers', subcat1_name: 'Laptops', subcat2: 'Acer'},
      {cat_id: 25, cat_name: 'Laptops and Computers', subcat1_name: 'Laptops', subcat2: 'HP(Hewlett Packard)'},
      {cat_id: 26, cat_name: 'Laptops and Computers', subcat1_name: 'Laptops', subcat2: 'Lenovo'},
      {cat_id: 27, cat_name: 'Laptops and Computers', subcat1_name: 'Laptops', subcat2: 'Dell'},
      {cat_id: 28, cat_name: 'Laptops and Computers', subcat1_name: 'Laptops', subcat2: 'Apple'},
      {cat_id: 29, cat_name: 'Laptops and Computers', subcat1_name: 'Accessories for laptops and PCs', subcat2: 'USB flash memory'},
      {cat_id: 30, cat_name: 'Laptops and Computers', subcat1_name: 'Accessories for laptops and PCs', subcat2: 'Laptop Bags & Backpacks'},
      {cat_id: 31, cat_name: 'Laptops and Computers', subcat1_name: 'Accessories for laptops and PCs', subcat2: 'Stands and tables for laptops'},
      {cat_id: 32, cat_name: 'Laptops and Computers', subcat1_name: 'Accessories for laptops and PCs', subcat2: 'Webcams'},
      {cat_id: 33, cat_name: 'Laptops and Computers', subcat1_name: 'Accessories for laptops and PCs', subcat2: 'Universal Mobile laptop Batteries'},
      {cat_id: 34, cat_name: 'Laptops and Computers', subcat1_name: 'Accessories for laptops and PCs', subcat2: 'Cables and adapters'},
      {cat_id: 35, cat_name: 'Smartphones, TV and electronics', subcat1_name: 'Popular Categoires', subcat2: 'Cell phones'},
      {cat_id: 36, cat_name: 'Smartphones, TV and electronics', subcat1_name: 'Popular Categoires', subcat2: 'TV sets'},
      {cat_id: 37, cat_name: 'Smartphones, TV and electronics', subcat1_name: 'Popular Categoires', subcat2: 'Cameras'},
      {cat_id: 38, cat_name: 'Smartphones, TV and electronics', subcat1_name: 'Telephones', subcat2: 'Smartphones'},
      {cat_id: 39, cat_name: 'Smartphones, TV and electronics', subcat1_name: 'Telephones', subcat2: 'Push-button telephones'},
      {cat_id: 40, cat_name: 'Smartphones, TV and electronics', subcat1_name: 'Telephones', subcat2: 'Office Phones'},
      {cat_id: 41, cat_name: 'Smartphones, TV and electronics', subcat1_name: 'Mobile phone accessories', subcat2: 'Cell phones'},
      {cat_id: 42, cat_name: 'Smartphones, TV and electronics', subcat1_name: 'Mobile phone accessories', subcat2: 'Cell phones'},
      {cat_id: 43, cat_name: 'Smartphones, TV and electronics', subcat1_name: 'Mobile phone accessories', subcat2: 'Cell phones'},
      {cat_id: 44, cat_name: 'Products for gamers', subcat1_name: 'Popular Categoires', subcat2: 'Playstation'},
      {cat_id: 45, cat_name: 'Products for gamers', subcat1_name: 'Popular Categoires', subcat2: 'PlayStation 5 game consoles'},
      {cat_id: 46, cat_name: 'Products for gamers', subcat1_name: 'Popular Categoires', subcat2: 'Gaming laptops'},
      {cat_id: 47, cat_name: 'Products for gamers', subcat1_name: 'Popular Categoires', subcat2: 'Hits PlayStation'},
      {cat_id: 48, cat_name: 'Products for gamers', subcat1_name: 'Gaming computers', subcat2: 'ARTLINE'},
      {cat_id: 49, cat_name: 'Products for gamers', subcat1_name: 'Gaming computers', subcat2: 'QUBE'},
      {cat_id: 50, cat_name: 'Products for gamers', subcat1_name: 'Gaming computers', subcat2: 'Cobra'},
      {cat_id: 51, cat_name: 'Applicances', subcat1_name: 'Popular Categoires', subcat2: 'Refrigerators'},
      {cat_id: 52, cat_name: 'Applicances', subcat1_name: 'Popular Categoires', subcat2: 'Air conditioners'},
      {cat_id: 53, cat_name: 'Applicances', subcat1_name: 'Popular Categoires', subcat2: 'Boilers'},
      {cat_id: 54, cat_name: 'Applicances', subcat1_name: 'Kitchen', subcat2: 'Multicooker'},
      {cat_id: 55, cat_name: 'Applicances', subcat1_name: 'Kitchen', subcat2: 'microwaves'},
      {cat_id: 56, cat_name: 'Household products', subcat1_name: 'Popular Categories', subcat2: 'ARTLINE'},
      {cat_id: 57, cat_name: 'Tools and car goods', subcat1_name: 'Popular Categories', subcat2: 'ARTLINE'},
      {cat_id: 58, cat_name: 'Plumbing and repair', subcat1_name: 'Popular Categories', subcat2: 'ARTLINE'},
      {cat_id: 59, cat_name: 'Cottage, garden and vegetable', subcat1_name: 'Popular Categories', subcat2: 'ARTLINE'},
      {cat_id: 60, cat_name: 'Sports, hobbies', subcat1_name: 'Popular Categories', subcat2: 'ARTLINE'},
      {cat_id: 61, cat_name: 'Clothes, shoes and jewelry', subcat1_name: 'Popular Categories', subcat2: 'ARTLINE'},
      {cat_id: 62, cat_name: 'Health and beauty', subcat1_name: 'Popular Categories', subcat2: 'ARTLINE'},
      {cat_id: 63, cat_name: "Children's products", subcat1_name: 'Popular Categories', subcat2: 'ARTLINE'},
      {cat_id: 64, cat_name: 'Stationery', subcat1_name: 'Popular Categories', subcat2: 'ARTLINE'},{cat_id: 1, cat_name: 'Household', subcat1_name: 'Popular Categories', subcat2: 'ARTLINE'},
      {cat_id: 65, cat_name: 'Alcoholic drinks and food', subcat1_name: 'Popular Categories', subcat2: 'ARTLINE'},{cat_id: 1, cat_name: 'Household', subcat1_name: 'Popular Categories', subcat2: 'ARTLINE'},
      {cat_id: 66, cat_name: 'Business', subcat1_name: 'Popular Categories', subcat2: 'ARTLINE'},{cat_id: 1, cat_name: 'Household', subcat1_name: 'Popular Categories', subcat2: 'ARTLINE'},
      {cat_id: 67, cat_name: 'Services and services', subcat1_name: 'Popular Categories', subcat2: 'ARTLINE'},{cat_id: 1, cat_name: 'Household', subcat1_name: 'Popular Categories', subcat2: 'ARTLINE'},
      {cat_id: 68, cat_name: "New Year's decor", subcat1_name: 'Popular Categories', subcat2: 'ARTLINE'},{cat_id: 1, cat_name: 'Household', subcat1_name: 'Popular Categories', subcat2: 'ARTLINE'},
      


    ];
    return {product_list};
  }
  
}
