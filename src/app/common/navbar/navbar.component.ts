import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  
  constructor() {
    
  }

  city: String;
  login: Boolean = true;
  register: Boolean;
  forgot: Boolean = false;
  toregister(): void {
    this.login = false;
    this.register = true;
    this.forgot = false;
  }
  tologin(): void {
    this.login=true;
    this.register = false;
    this.forgot = false;
  }
  ngOnInit(): void {
    
  }

}

