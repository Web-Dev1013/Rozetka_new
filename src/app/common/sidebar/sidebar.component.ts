import { Component, OnInit } from '@angular/core';
import { ProductListService} from '../../service/product-list.service';
import { Product } from '../../model/product';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {
  productlist: Product[];
  constructor(private productlistService: ProductListService) { }

  catgories: any[] = [
    {name: 'Laptops and Comupters', id: 'Lap'},
    {name: 'Smartphones, TV and electronics', id: 'ele'},
    {name: 'Products for gamers', id: 'gamer'},
    {name: 'Appliances', id: 'app'},
    {name: 'Household products', id: 'house'},
    {name: 'Tools and car goods', id: 'car'},
    {name: 'Plumbing and repair', id: 'plumbing'},
    {name: 'Cottage, garden and vegetable', id: 'vegetable'},
    {name: 'Sports and hobbies', id: 'sports'},
    {name: 'Clothes, shoes and jewelry', id: 'clothes'},
    {name: 'Health and beauty', id: 'health'},
    {name: "Children's products", id: 'child'},
    {name: 'Stationery and books', id: 'stationery'},
    {name: 'Alcoholic drinks and food', id: 'alcoholic'},
    {name: 'Business goods', id: 'business'},
    {name: 'Services and services', id: 'service'},
    {name: "New Year's decor", id: 'new-year'}
  ];
  ngOnInit(): void {
    this.productlistService.getProducts_list().subscribe((products: Product[]) => {
      this.productlist = products;
      console.log(products);
    })
  }

}
