import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainComponent } from './pages/main/main.component';
import { PremiumComponent } from './pages/premium/premium.component';
import { QuestionsComponent } from './pages/product-details/questions/questions.component';
import { HelpComponent } from './pages/help/help.component';



const routes: Routes = [
  {path: '', redirectTo: 'main', pathMatch: 'full'},
  {path: 'main', component: MainComponent },
  {path: 'questions', component: QuestionsComponent },
  {path: 'help', component: HelpComponent },
  {path: 'premium', component: PremiumComponent },
  {path: 'product/:id', loadChildren: () => import(`./pages/product-details/product-details.module`).then(m =>m.ProductDetailsModule)},
  {path: 'resource', loadChildren: () => import(`./pages/resource/resource.module`).then(m=>m.ResourceModule)},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
