import { Component } from '@angular/core';
import { Location } from '@angular/common';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'ecommerce';

  constructor(public location: Location) {

  }

  removeCarousel() {
    var titlee = this.location.prepareExternalUrl(this.location.path());
    titlee = titlee.slice(1);
    if(titlee.indexOf("product/") >-1 || titlee.indexOf("resource") > -1 || titlee.indexOf("premium") > -1 || titlee.indexOf("help") > -1) {
      return false;
    }
    else {
      return true;
    }
  }

  removeSideNavbar() {
    var titlee = this.location.prepareExternalUrl(this.location.path());
    titlee = titlee.slice(1);
    if(titlee.indexOf("resource/") > -1 || titlee.indexOf("product/") > -1) {
      return false;
    }
    else {
      return true;
    }
  }

  removeFooter() {
    var titlee = this.location.prepareExternalUrl(this.location.path());
    titlee = titlee.slice(1);
    if(titlee.indexOf("main") > -1) {
      return false;
    }
    else {
      return true;
    }
  }

  scrollToElement($element): void {
    console.log($element);
    $element.scrollIntoView({behavior: "smooth", block: "start", inline: "nearest"});
  }
}
