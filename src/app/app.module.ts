import { BrowserModule } from '@angular/platform-browser';
// import { NgModule } from '@angular/core';
import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core'; 
import { AppComponent } from './app.component';
import { SidebarComponent } from './common/sidebar/sidebar.component';
import { NavbarComponent } from './common/navbar/navbar.component';
import { FooterComponent } from './common/footer/footer.component';
import { CarouselComponent } from './common/carousel/carousel.component';
import { MainComponent } from './pages/main/main.component';
import { HttpClientModule } from '@angular/common/http';
import { InMemoryWebApiModule } from 'angular-in-memory-web-api';
import { DataService } from './service/data.service';
import { fromEventPattern } from 'rxjs';
import { PremiumComponent } from './pages/premium/premium.component';
import { AppRoutingModule } from './app-routing.module';
import { HelpComponent } from './pages/help/help.component';


@NgModule({
  declarations: [
    AppComponent,
    SidebarComponent,
    NavbarComponent,
    FooterComponent,
    CarouselComponent,
    MainComponent,
    PremiumComponent,
    HelpComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    InMemoryWebApiModule.forRoot(DataService),
    
  ],
  providers: [],
  bootstrap: [AppComponent]
})

export class AppModule { }
