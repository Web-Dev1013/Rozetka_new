export class Product {
    cat_id: number;
    cat_name: string;
    subcat1: string;
    subcat2: string;

    constructor( cat_id, cat_name, subcat1, subcat2) {
        this.cat_id = cat_id;
        this.cat_name = cat_name;
        this.subcat1 = subcat1;
        this.subcat2 = subcat2
    }
}
